package com.test.weatherdata.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.weatherdata.model.*;
import com.test.weatherdata.repository.WeatherRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;


@Service
@Configuration
public class WeatherService {

    private final WeatherRepository weatherRepository;
    public WeatherService(WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    @Bean
    public WeatherService getweatherService(){

        return new WeatherService(weatherRepository);
    }

    public Iterable<WeatherData> findAll() {

        try {
            return weatherRepository.findAll();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public WeatherData save(WeatherData weatherData){
       return weatherRepository.save(weatherData);
    }


    public WeatherData mapJSONtoJava(WeatherData weatherData){
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writeValueAsString(weatherData);

            WeatherData weatherDataObj = mapper.readValue(jsonString, WeatherData.class);
            System.out.println("----------------Saving- data -------------------------------------------------------------------");
            System.out.println(weatherDataObj.toString());
            WeatherData resultData =  weatherRepository.save(weatherDataObj);
            System.out.println("----------------Saved- -------------------------------------------------------------------");

            return resultData;

        }catch (Exception e){
            e.printStackTrace();
        }

        return weatherData;
    }



}



