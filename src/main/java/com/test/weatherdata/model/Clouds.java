package com.test.weatherdata.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Data
//@Entity
@Embeddable
public class Clouds {

//    @Id
@JsonProperty("all")
private Integer allclouds;

    public Integer getAllclouds() {
        return allclouds;
    }

    public void setAllclouds(Integer allclouds) {
        this.allclouds = allclouds;
    }

    public Clouds(Integer allclouds) {
        this.allclouds = allclouds;
    }

    public Clouds() {
    }
}