package com.test.weatherdata.model;


import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Data
//@Entity
@Embeddable
public class Wind {


    public Wind(Double speed, Double deg, Double gust) {
        this.speed = speed;
        this.deg = deg;
        this.gust = gust;
    }

//    @Id
    private Double speed;
    private Double deg;
    private Double gust;
    public Wind() {
    }


}