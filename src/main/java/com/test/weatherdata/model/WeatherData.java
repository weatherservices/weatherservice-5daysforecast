package com.test.weatherdata.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class WeatherData {

//    private Long id;

    private String cod;
    private Integer message;
    @Id
    private Integer cnt;


    @OneToMany( cascade = CascadeType.ALL)
    private List<ForecastList> list;

    @OneToOne(cascade = CascadeType.ALL)
    private City city;



    public WeatherData() {}
}



