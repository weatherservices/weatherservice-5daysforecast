package com.test.weatherdata.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Data

@Entity
public class Coord {

    public Long getLon() {
        return lon;
    }

    public void setLon(Long lon) {
        this.lon = lon;
    }

    public Long getLat() {
        return lat;
    }

    public void setLat(Long lat) {
        this.lat = lat;
    }

    public Coord(Long lon, Long lat) {
        this.lon = lon;
        this.lat = lat;
    }

    @Id
    private Long lon;
    private Long lat;
    public Coord() {
    }
}