package com.test.weatherdata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.weatherdata.model.Coord;
import com.test.weatherdata.model.WeatherData;
import com.test.weatherdata.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@RestController
@RequestMapping("/weathers")
public class WeatherController {
    @Autowired
    private WeatherService weatherService;

    @Autowired
    private RestTemplate restTemplate;

    public WeatherController(WeatherService weatherService) {// --->as we have used @AllArgsConstructor
        this.weatherService = weatherService;
    }

    /**
    DESC - fetch all forecast weather data from repository
    PATH -  /all
    params - NONE
    REQUEST - GET
    * */
    @GetMapping("/all")
    public Iterable<WeatherData> findAll() {
        return weatherService.findAll();
    }


   /* *
   DESC - save the forecast data fetched from 3rd party API to repository
   PATH -  /getall
   params - NONE
   REQUEST - GET
   * */
    @GetMapping("/getall")
    public WeatherData getAllFromApi() {
        WeatherData weather = restTemplate.getForObject("http://api.openweathermap.org/data/2.5/forecast?id=524901&appid=13099d0eb0008fa1bf2eb0802a786b34", WeatherData.class);
        return weatherService.mapJSONtoJava(weather);
    }


}
